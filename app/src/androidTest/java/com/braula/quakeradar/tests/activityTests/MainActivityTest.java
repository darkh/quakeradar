package com.braula.quakeradar.tests.activityTests;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.braula.quakeradar.R;
import com.braula.quakeradar.activities.MainActivity;
import com.braula.quakeradar.adapters.QuakeAdapter;
import com.braula.quakeradar.enums.TimeFrame;

/**
 * Created by Aleksander_Braula on 15-Dec-14.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2 {
    public MainActivityTest() {
        super(MainActivity.class);
    }

    private MainActivity activity;

    private ListView quakeListView;
    private QuakeAdapter quakeAdapter;

    private ProgressBar loadingIndicator;
    private TextView infoText;
    private Spinner timeSpinner;
    private Switch tsunamiSwitch;


    @Override
    protected void setUp() throws Exception {
        super.setUp();
        activity = (MainActivity)getActivity();

        loadingIndicator = (ProgressBar) activity.findViewById(R.id.loadingIndicator);
        infoText = (TextView) activity.findViewById(R.id.infoText);
        timeSpinner = (Spinner) activity.findViewById(R.id.timeSpinner);
        tsunamiSwitch = (Switch) activity.findViewById(R.id.tsunamiSwitch);

        quakeListView = (ListView) activity.findViewById(R.id.quakeListView);
        quakeAdapter = (QuakeAdapter)quakeListView.getAdapter();
    }

    public void testInit() throws Exception {
        assertNotNull("Loading indicator not initialized", loadingIndicator);
        assertNotNull("Info TextView not initialized", infoText);
        assertNotNull("Timeframe Spinner not initialized", timeSpinner);
        assertNotNull("Tsunami Switch not initialized", tsunamiSwitch);
        assertNotNull("ListView not initialized", quakeListView);
        assertNotNull("Adapter not initialized", quakeAdapter);
    }

    public void testTsunamiSwitch() {
        boolean initialState = tsunamiSwitch.isChecked();
        assertTrue("Tsunami switch not visible", tsunamiSwitch.getVisibility() == View.VISIBLE);
        TouchUtils.clickView(this, tsunamiSwitch);
        assertTrue("Tsunami switch state not changed", tsunamiSwitch.isChecked() != initialState);
    }

    public void testTimeFrameSpinner() {
        assertNotNull("Spinner listener set", timeSpinner.getOnItemSelectedListener());
        assertTrue("Spinner item count incorrect", timeSpinner.getAdapter().getCount() == TimeFrame.values().length);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        activity = null;
    }
}
