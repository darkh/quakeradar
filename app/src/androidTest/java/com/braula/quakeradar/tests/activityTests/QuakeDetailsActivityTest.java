package com.braula.quakeradar.tests.activityTests;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.test.ActivityInstrumentationTestCase2;

import com.braula.quakeradar.R;
import com.braula.quakeradar.activities.QuakeDetailsActivity;
import com.braula.quakeradar.fragments.QuakeMapFragment;
import com.braula.quakeradar.fragments.QuakeTweetsFragment;

/**
 * Created by Aleksander_Braula on 15-Dec-14.
 */
public class QuakeDetailsActivityTest extends ActivityInstrumentationTestCase2 {
    public QuakeDetailsActivityTest() {
        super(QuakeDetailsActivity.class);
    }

    private QuakeDetailsActivity activity;

    private Double latitude = 37.781157;
    private Double longitude = -122.398720;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        activity = (QuakeDetailsActivity)getActivity();
    }

    private Fragment startFragment(Fragment fragment) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container, fragment, "tag");
        transaction.commit();
        getInstrumentation().waitForIdleSync();
        return fragment.getFragmentManager().findFragmentByTag("tag");
    }

    public void testQuakeTweetsFragment() {
        QuakeTweetsFragment quakeTweetsFragment = new QuakeTweetsFragment();
        quakeTweetsFragment.setCoordinates(latitude, longitude);

        Fragment fragment = startFragment(quakeTweetsFragment);

        assertNotNull("Fragment is null", fragment);
    }

    public void testQuakeMapFragment() {
        QuakeMapFragment quakeMapFragment = new QuakeMapFragment();
        quakeMapFragment.setCoordinates(latitude, longitude);

        Fragment fragment = startFragment(quakeMapFragment);

        assertNotNull("Fragment is null", fragment);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
        activity = null;
    }
}
