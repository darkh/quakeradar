package com.braula.quakeradar.tests.adapterTests;

import android.test.AndroidTestCase;
import android.view.View;
import android.widget.TextView;

import com.braula.quakeradar.R;
import com.braula.quakeradar.adapters.QuakeAdapter;
import com.braula.quakeradar.model.Quake;
import com.braula.quakeradar.model.QuakeProperties;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Aleksander_Braula on 15-Dec-14.
 */
public class QuakeAdapterTest extends AndroidTestCase {

    private QuakeAdapter quakeAdapter;

    private Quake quake1;
    private Quake quake2;
    private Quake quakeWNulls;

    private Double latitude = 37.781157;
    private Double longitude = -122.398720;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ArrayList<Quake> quakeList = new ArrayList<Quake>();

        QuakeProperties properties = new QuakeProperties();
        properties.setRegion("Quake 1 place");

        quake1 = new Quake();
        quake1.setId("quake1");
        quake1.setProperties(properties);
        quake1.setTsunami("1");
        quake1.setMagnitude(4.2);
        quake1.setTime(1234567890);
        quake1.setLongitude(latitude);
        quake1.setLatitude(longitude);

        quakeList.add(quake1);

        properties = new QuakeProperties();
        properties.setRegion("Quake 2 place");

        quake2 = new Quake();
        quake2.setId("quake2");
        quake2.setProperties(properties);
        quake2.setTsunami(null);
        quake2.setMagnitude(4.24);
        quake2.setTime(12345678024l);
        quake2.setLongitude(longitude);
        quake2.setLatitude(latitude);

        quakeList.add(quakeWNulls);

        properties = new QuakeProperties();
        properties.setRegion(null);

        quakeWNulls = new Quake();
        quakeWNulls.setId("quake2");
        quakeWNulls.setProperties(properties);
        quakeWNulls.setTsunami(null);
        quakeWNulls.setMagnitude(4.24);
        quakeWNulls.setTime(12345678024l);
        quakeWNulls.setLatitude(latitude);

        quakeList.add(quakeWNulls);

        quakeAdapter = new QuakeAdapter(getContext());
        quakeAdapter.setQuakeList(quakeList);
    }

    public void testGetItem() {
        assertEquals("quake 1 place was expected.", quake1.getProperties().getRegion(), ((Quake) quakeAdapter.getItem(0)).getProperties().getRegion());
    }

    public void testGetItemId() {
        assertEquals("Wrong ID.", 0, quakeAdapter.getItemId(0));
    }

    public void testGetCount() {
        assertEquals("Contacts amount incorrect.", 3, quakeAdapter.getCount());
    }

    public void testGetView() {
        View view = quakeAdapter.getView(0, null, null);

        TextView quakePlaceText = (TextView) view.findViewById(R.id.quakePlaceText);
        TextView quakeDateText = (TextView) view.findViewById(R.id.quakeDateText);
        TextView quakeMagnitudeText = (TextView) view.findViewById(R.id.quakeMagnitudeText);

        assertNotNull("View is null. ", view);
        assertNotNull("Place TextView is null. ", quakePlaceText);
        assertNotNull("Date TextView is null. ", quakeDateText);
        assertNotNull("Magnitude TextView is null. ", quakeMagnitudeText);

        assertEquals("Place doesn't match.", quake1.getProperties().getRegion(), quakePlaceText.getText());
        assertEquals("Date doesn't match.", quake1.getStringDate(), quakeDateText.getText());
        assertEquals("Magnitude doesn't match.", (new DecimalFormat("#0.0")).format(quake1.getMagnitude()), quakeMagnitudeText.getText());
    }

    public void testGetViewWNull() {
        View view = quakeAdapter.getView(2, null, null);

        TextView quakePlaceText = (TextView) view.findViewById(R.id.quakePlaceText);
        TextView quakeDateText = (TextView) view.findViewById(R.id.quakeDateText);
        TextView quakeMagnitudeText = (TextView) view.findViewById(R.id.quakeMagnitudeText);

        assertNotNull("View is null. ", view);
        assertNotNull("Place TextView is null. ", quakePlaceText);
        assertNotNull("Date TextView is null. ", quakeDateText);
        assertNotNull("Magnitude TextView is null. ", quakeMagnitudeText);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
        quakeAdapter = null;
    }
}
