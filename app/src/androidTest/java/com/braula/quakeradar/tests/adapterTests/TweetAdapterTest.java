package com.braula.quakeradar.tests.adapterTests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import android.test.AndroidTestCase;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.braula.quakeradar.R;
import com.braula.quakeradar.adapters.TweetAdapter;

import java.util.ArrayList;
import java.util.Calendar;

import twitter4j.Status;
import twitter4j.User;

/**
 * Created by Aleksander_Braula on 15-Dec-14.
 */
public class TweetAdapterTest extends AndroidTestCase {

    private TweetAdapter tweetAdapter;

    private Status status1;
    private Status status2;
    private Status statusWNulls;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ArrayList<Status> tweetList = new ArrayList<Status>();

        User user = mock(User.class);
        when(user.getName()).thenReturn("user");

        status1 = mock(Status.class);

        when(status1.getText()).thenReturn("Status 1 text");
        when(status1.getUser()).thenReturn(user);
        when(status1.getCreatedAt()).thenReturn(Calendar.getInstance().getTime());

        tweetList.add(status1);

        status2 = mock(Status.class);
        when(status2.getText()).thenReturn("Status 2 text");
        when(status2.getUser()).thenReturn(user);
        when(status2.getCreatedAt()).thenReturn(Calendar.getInstance().getTime());

        tweetList.add(status2);

        statusWNulls = mock(Status.class);
        when(statusWNulls.getText()).thenReturn(null);
        when(statusWNulls.getUser()).thenReturn(user);
        when(statusWNulls.getCreatedAt()).thenReturn(Calendar.getInstance().getTime());

        tweetList.add(statusWNulls);

        tweetAdapter = new TweetAdapter(getContext());
        tweetAdapter.setTweetList(tweetList);
    }

    public void testGetItem() {
        assertEquals("Tweet 1 text was expected.", status1.getText(), ((Status) tweetAdapter.getItem(0)).getText());
    }

    public void testGetItemId() {
        assertEquals("Wrong ID.", 0, tweetAdapter.getItemId(0));
    }

    public void testGetCount() {
        assertEquals("Contacts amount incorrect.", 3, tweetAdapter.getCount());
    }

    public void testGetView() {
        View view = tweetAdapter.getView(0, null, null);

        TextView tweetText = (TextView) view.findViewById(R.id.tweetText);
        TextView tweeterText = (TextView) view.findViewById(R.id.tweeterText);
        TextView tweetDateText = (TextView) view.findViewById(R.id.tweetDateText);
        ImageView tweeterIcon = (ImageView) view.findViewById(R.id.tweeterIcon);

        assertNotNull("View is null. ", view);
        assertNotNull("Tweet TextView is null. ", tweetText);
        assertNotNull("Tweeter TextView is null. ", tweeterText);
        assertNotNull("Date TextView is null. ", tweetDateText);
        assertNotNull("Twitter ImageView is null. ", tweeterIcon);

        assertEquals("Text doesn't match.", status1.getText(), tweetText.getText());
        assertEquals("User doesn't match.", "@" +status1.getUser().getName(), tweeterText.getText());
    }

    public void testGetViewWNull() {
        View view = tweetAdapter.getView(2, null, null);

        TextView tweetText = (TextView) view.findViewById(R.id.tweetText);
        TextView tweeterText = (TextView) view.findViewById(R.id.tweeterText);
        TextView tweetDateText = (TextView) view.findViewById(R.id.tweetDateText);
        ImageView tweeterIcon = (ImageView) view.findViewById(R.id.tweeterIcon);

        assertNotNull("View is null. ", view);
        assertNotNull("Tweet TextView is null. ", tweetText);
        assertNotNull("Tweeter TextView is null. ", tweeterText);
        assertNotNull("Date TextView is null. ", tweetDateText);
        assertNotNull("Twitter ImageView is null. ", tweeterIcon);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
        tweetAdapter = null;
    }
}
