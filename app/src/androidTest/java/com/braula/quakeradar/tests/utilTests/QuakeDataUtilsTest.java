package com.braula.quakeradar.tests.utilTests;

import android.test.AndroidTestCase;

import com.braula.quakeradar.model.Quake;
import com.braula.quakeradar.utils.QuakeDataUtils;

import java.util.ArrayList;

/**
 * Created by Aleksander_Braula on 15-Dec-14.
 */
public class QuakeDataUtilsTest extends AndroidTestCase {

    public void testDataAcquisition() {
        ArrayList<Quake> quakeList = QuakeDataUtils.tryToGetQuakeList();
        assertFalse("No quake data downloaded", quakeList.isEmpty());
    }
}
