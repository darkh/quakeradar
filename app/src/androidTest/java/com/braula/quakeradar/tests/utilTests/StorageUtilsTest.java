package com.braula.quakeradar.tests.utilTests;

import android.test.AndroidTestCase;

import com.braula.quakeradar.R;
import com.braula.quakeradar.utils.StorageUtils;

/**
 * Created by Aleksander_Braula on 15-Dec-14.
 */
public class StorageUtilsTest extends AndroidTestCase {

    private String testString = "Test string.";
    private String testNameString = "testname";

    public void testStoreAndRetrieve() {

        StorageUtils.putStringInSharedPreferences(testNameString, testString);

        String prefsString = StorageUtils.getStringFromSharedPreferences(testNameString);

        assertEquals("Wrong item retrieved", testString, prefsString);
    }

    public void testStringResourceRetrieve() {
        String resourceString = StorageUtils.getStringResource(R.string.app_name);

        assertEquals("Wrong resource string", resourceString, "QuakeRadar");
    }
}
