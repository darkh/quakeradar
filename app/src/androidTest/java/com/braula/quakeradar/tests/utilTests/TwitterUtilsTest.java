package com.braula.quakeradar.tests.utilTests;

import android.test.AndroidTestCase;

import com.braula.quakeradar.utils.TwitterUtils;

import java.util.List;

import twitter4j.GeoLocation;
import twitter4j.Status;

/**
 * Created by Aleksander_Braula on 15-Dec-14.
 */
public class TwitterUtilsTest extends AndroidTestCase {

    private Double latitude = 37.781157;
    private Double longitude = -122.398720;

    public void testDataAcquisition() {
        GeoLocation geoLocation = new GeoLocation(latitude, longitude);

        List<Status> tweetList = TwitterUtils.tryToGetTweetListByGeoLocation(geoLocation);

        assertTrue("Tweet count 0", tweetList.size() > 0);
        assertTrue("Tweet count over 20", tweetList.size() <= 20);
    }
}
