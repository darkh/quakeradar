package com.braula.quakeradar.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.braula.quakeradar.R;
import com.braula.quakeradar.adapters.QuakeAdapter;
import com.braula.quakeradar.consts.Constants;
import com.braula.quakeradar.enums.TimeFrame;
import com.braula.quakeradar.model.Quake;
import com.braula.quakeradar.utils.QuakeDataUtils;

import com.crashlytics.android.Crashlytics;

import eu.darkh.library.utils.NetworkUtils;
import eu.darkh.library.utils.StorageUtils;
import eu.darkh.library.utils.UiUtils;
import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;

/**
 * Main activity class
 * Created by Aleksander_Braula on 2014-12-12.
 */
public class MainActivity extends ActionBarActivity {

    private static final String SHARED_PREFERENCES_FILE = "quakedata";


    private ListView quakeListView;
    private QuakeAdapter quakeAdapter;

    private ProgressBar loadingIndicator;
    private TextView infoText;

    private TimeFrame filterTimeframe = TimeFrame.DAYS_7;
    private boolean isFilterTsunami = false;

    private final int startingPosition = filterTimeframe.ordinal();

    private boolean isDoubleBackToExitPressedOnce = false;

    private LoadQuakeDataTask loadQuakeDataTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupCrashlytics();
        StorageUtils.initStorageUtils(getBaseContext(), SHARED_PREFERENCES_FILE);

        setContentView(R.layout.activity_main);

        UiUtils.setContext(this);

        setupUIElements();

        launchLoaderTask();
    }

    private void setupCrashlytics() {
        if (!Constants.IS_DEVELOPMENT) {
            Fabric.with(this, new Crashlytics());
        }
    }

    private void setupUIElements(){
        loadingIndicator = (ProgressBar) findViewById(R.id.loadingIndicator);
        infoText = (TextView) findViewById(R.id.infoText);

        setupSpinner();
        setupListView();
    }

    private void setupSpinner() {
        ArrayAdapter<TimeFrame> playerAdapter =
                new ArrayAdapter<TimeFrame>(this, android.R.layout.simple_spinner_item, TimeFrame.values());

        playerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner timeSpinner = (Spinner) findViewById(R.id.timeSpinner);
        timeSpinner.setAdapter(playerAdapter);
        timeSpinner.setSelection(startingPosition);
        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setTimeframeAndFilter(TimeFrame.values()[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void setTimeframeAndFilter(TimeFrame timeframe){
        filterTimeframe = timeframe;
        filterQuakeData();
    }

    public void setTsunamiAndFilter(View view) {
        isFilterTsunami = ((Switch) view).isChecked();
        filterQuakeData();
    }

    private void filterQuakeData() {
        quakeAdapter.filterQuakeList(isFilterTsunami, filterTimeframe);
    }

    private void setupListView(){
        quakeAdapter = new QuakeAdapter(this);

        quakeListView = (ListView) findViewById(R.id.quakeListView);
        quakeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(NetworkUtils.hasNetworkConnection()) {
                    launchQuakeDetailsActivity(position);
                } else {
                    UiUtils.showToastWithText(R.string.quake_details_offline);
                }
            }
        });

        quakeListView.setAdapter(quakeAdapter);
    }

    private void launchLoaderTask() {
        cancelLoaderIfRunning();
        loadQuakeDataTask = new LoadQuakeDataTask();
        loadQuakeDataTask.execute();
    }

    private void cancelLoaderIfRunning() {
        if (loadQuakeDataTask != null && loadQuakeDataTask.getStatus() == AsyncTask.Status.RUNNING) {
            loadQuakeDataTask.cancel(true);
            loadQuakeDataTask = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (isDoubleBackToExitPressedOnce) {
            finish();
            return;
        }

        this.isDoubleBackToExitPressedOnce = true;

        UiUtils.showToastWithText(R.string.double_back_exit);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isDoubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh_data) {
            Log.d("QR", "Refreshing data");
            launchLoaderTask();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelLoaderIfRunning();
    }

    private void launchQuakeDetailsActivity(int position) {
        Intent intent = prepareQuakeDetailsIntent(position);
        startActivity(intent);
    }

    private Intent prepareQuakeDetailsIntent(int position) {
        Quake quake = (Quake) quakeAdapter.getItem(position);

        Intent intent = new Intent(getBaseContext(), QuakeDetailsActivity.class);
        intent.putExtra("latitude", quake.getLatitude());
        intent.putExtra("longitude", quake.getLongitude());
        intent.putExtra("place", quake.getProperties().getRegion());

        return intent;
    }

    private class LoadQuakeDataTask extends AsyncTask<Void, Void, ArrayList<Quake>> {

        protected void onPreExecute (){
            showLoading();

            if (!NetworkUtils.hasNetworkConnection()) {
                UiUtils.showToastWithText(R.string.stale_data_warning);
            }
        }

        protected ArrayList<Quake> doInBackground(Void... params) {
            return QuakeDataUtils.tryToGetQuakeList();
        }

        protected void onPostExecute(final ArrayList<Quake> quakeList) {
            Log.d("QR", "Quake no " + quakeList.size());

            hideLoading();

            if (quakeList.size() > 0) {
                quakeAdapter.setQuakeList(quakeList);
            } else {
                showNoDataText();
            }
        }
    }

    private void showLoading() {
        hideListView();
        showLoadingIndicator();
        hideNoDataText();
    }

    private void hideLoading() {
        hideLoadingIndicator();
        showListView();
    }

    private void showListView(){
        quakeListView.setVisibility(View.VISIBLE);
    }

    private void hideListView(){
        quakeListView.setVisibility(View.GONE);
    }

    private void showNoDataText(){
        infoText.setVisibility(View.VISIBLE);
    }

    private void hideNoDataText(){
        infoText.setVisibility(View.GONE);
    }

    private void showLoadingIndicator() {
        loadingIndicator.setVisibility(View.VISIBLE);
    }

    private void hideLoadingIndicator() {
        loadingIndicator.setVisibility(View.GONE);
    }
}

