package com.braula.quakeradar.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.braula.quakeradar.R;
import com.braula.quakeradar.fragments.QuakeMapFragment;
import com.braula.quakeradar.fragments.QuakeTweetsFragment;

/**
 * Details activity class with tweet and map fragments
 * Created by Aleksander_Braula on 2014-12-12.
 */
public class QuakeDetailsActivity extends ActionBarActivity {

    private QuakeTweetsFragment quakeTweetsFragment = null;
    private QuakeMapFragment quakeMapFragment = null;

    private ToggleButton flipToggleButton;

    private double latitude;
    private double longitude;
    private String place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quake_details);

        extractIntentExtras();
        setTitle(place);

        if (savedInstanceState == null) {
            changeToTweetsFragment();
        }

    }

    private void extractIntentExtras(){
        latitude = getIntent().getDoubleExtra("latitude", 0.0);
        longitude = getIntent().getDoubleExtra("longitude", 0.0);
        place = getIntent().getStringExtra("place");

        Log.d("QR", "Coordinates: " + latitude + ", " + longitude);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_quake_details, menu);

        setupToggleButton(menu);

        return true;
    }

    private void setupToggleButton(Menu menu) {
        flipToggleButton = (ToggleButton) menu.getItem(0).getActionView().findViewById(R.id.flip_tooggle_button);
        flipToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean showMap) {
                if (showMap) {
                    changeToMapFragment();
                } else {
                    returnToPreviousFragment();
                }
            }
        });
    }

    private void changeToTweetsFragment(){
        initQuakeTweetFragment();

        Log.d("QR", "Loading tweet fragment");

        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, quakeTweetsFragment)
                .commit();
    }

    private void changeToMapFragment() {
        initQuakeMapFragment();

        Log.d("QR", "Loading map fragment");

        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.animator.card_flip_left_in,
                        R.animator.card_flip_left_out,
                        R.animator.card_flip_right_in,
                        R.animator.card_flip_right_out)
                .replace(R.id.fragment_container, quakeMapFragment)
                .addToBackStack(null)
                .commit();
    }

    private void returnToPreviousFragment(){
        getFragmentManager().popBackStack();
    }

    private void initQuakeMapFragment() {
        if (quakeMapFragment == null) {
            quakeMapFragment = new QuakeMapFragment();
            quakeMapFragment.setCoordinates(latitude, longitude);
        }
    }

    private void initQuakeTweetFragment() {
        if (quakeTweetsFragment == null) {
            quakeTweetsFragment = new QuakeTweetsFragment();
            quakeTweetsFragment.setCoordinates(latitude, longitude);
        }
    }

    public void onBackPressed() {
        if (!isBackStackEmpty()) {
            flipToggleButton.setChecked(false);
        } else {
            finish();
        }
    }

    private boolean isBackStackEmpty() {
        return getFragmentManager().getBackStackEntryCount() == 0;
    }
}
