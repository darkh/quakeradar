package com.braula.quakeradar.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.braula.quakeradar.R;
import com.braula.quakeradar.enums.QuakeMagnitude;
import com.braula.quakeradar.enums.TimeFrame;
import com.braula.quakeradar.model.Quake;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;

import org.joda.time.Interval;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for Quake items for list view
 * Created by Aleksander_Braula on 12-Dec-14.
 */
public class QuakeAdapter extends BaseAdapter {

    private final Context context;

    private List<Quake> quakeList = new ArrayList<Quake>();
    private List<Quake> baseQuakeList = new ArrayList<Quake>();

    private final RangeMap<Double, QuakeMagnitude> magnitudeColorRangeSet = TreeRangeMap.create();

    public QuakeAdapter(Context context) {
        super();
        this.context = context;
        configureMagnitudeColorRangeSet();
    }

    public void setQuakeList(ArrayList<Quake> quakeList) {
        this.quakeList = quakeList;
        this.baseQuakeList = quakeList;
        notifyDataSetChanged();
    }

    private void configureMagnitudeColorRangeSet(){
        magnitudeColorRangeSet.put(Range.lessThan(2.0), QuakeMagnitude.I);
        magnitudeColorRangeSet.put(Range.closedOpen(2.0, 4.0), QuakeMagnitude.II_III);
        magnitudeColorRangeSet.put(Range.closedOpen(4.0, 5.0), QuakeMagnitude.IV);
        magnitudeColorRangeSet.put(Range.closedOpen(5.0, 6.0), QuakeMagnitude.V);
        magnitudeColorRangeSet.put(Range.closedOpen(6.0, 7.0), QuakeMagnitude.VI);
        magnitudeColorRangeSet.put(Range.closedOpen(7.0, 9.0), QuakeMagnitude.VII_VIII);
        magnitudeColorRangeSet.put(Range.closedOpen(9.0, 10.0), QuakeMagnitude.IX);
        magnitudeColorRangeSet.put(Range.atLeast(10.0), QuakeMagnitude.X);
    }

    @Override
    public int getCount() {
        return quakeList.size();
    }

    @Override
    public Object getItem(int position) {
        return quakeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater li = LayoutInflater.from(context);
            view = li.inflate(R.layout.row_quake_item, null);
        }

        setupRowElements(view, position);

        return view;
    }

    private void setupRowElements(View view, int position) {

        Quake quake = quakeList.get(position);

        TextView quakePlaceText = (TextView)view.findViewById(R.id.quakePlaceText);
        quakePlaceText.setText(quake.getProperties().getRegion());

        TextView quakeDateText = (TextView)view.findViewById(R.id.quakeDateText);
        quakeDateText.setText(quake.getStringDate());

        setupMagnitudeTextView(view, quake.getMagnitude());

    }

    private void setupMagnitudeTextView(View view, double magnitude) {
        TextView quakeMagnitudeText = (TextView) view.findViewById(R.id.quakeMagnitudeText);
        quakeMagnitudeText.setText(formatDouble(magnitude));
        quakeMagnitudeText.setTextColor(getTextColorByMagnitude(magnitude));

        GradientDrawable bgShape = (GradientDrawable)quakeMagnitudeText.getBackground();
        bgShape.setColor(getViewColorByMagnitude(magnitude));
    }

    private String formatDouble(double number) {
        return (new DecimalFormat("#0.0")).format(number);
    }

    private int getTextColorByMagnitude(double magnitude) {
        return magnitude >= 9.0 ? Color.WHITE : Color.BLACK;
    }

    private int getViewColorByMagnitude(double magnitude) {
        QuakeMagnitude quakeMagnitude = magnitudeColorRangeSet.get(magnitude);
        return quakeMagnitude.getColor();
    }

    public void filterQuakeList(boolean isTsunamiOnly, TimeFrame timeframe) {

        Log.d("QR", "Filter tsunami only: " + isTsunamiOnly + ", time frame: " + timeframe.toString());

        Interval interval = timeframe.getInterval();

        FluentIterable<Quake> fluentIterable = FluentIterable.from(baseQuakeList);
        fluentIterable = fluentIterable.filter(byTimeFrame(interval));

        if (isTsunamiOnly) {
            fluentIterable = fluentIterable.filter(byTsunami);
        }

        quakeList = fluentIterable.toList();

        notifyDataSetChanged();
    }

    private Predicate<Quake> byTsunami = new Predicate<Quake>() {
        public boolean apply(Quake input) {
            return input.isTsunami();
        }
    };

    private Predicate<Quake> byTimeFrame(final Interval interval) {
        return new Predicate<Quake>() {
            @Override
            public boolean apply(Quake input) {
                return interval.contains(input.getDateTime());
            }
        };
    }
}
