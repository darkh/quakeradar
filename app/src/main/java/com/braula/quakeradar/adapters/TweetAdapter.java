package com.braula.quakeradar.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.braula.quakeradar.R;
import com.braula.quakeradar.animations.ResizeAnimation;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import eu.darkh.library.utils.StorageUtils;
import eu.darkh.library.utils.UiUtils;
import twitter4j.Status;

/**
 * Adapter for tweet Status items for list view
 * Created by Aleksander_Braula on 2014-12-12.
 */
public class TweetAdapter extends BaseAdapter {

    private Context context;

    private List<Status> tweetList = new ArrayList<Status>();

    private int lastOpenedPosition = -1;
    private View lastOpenedView;

    private final int DP_50 = UiUtils.convertIntToDp(50);
    private final int DP_75 = UiUtils.convertIntToDp(75);

    private LoadTweeterIconTask loadTweeterIconTask;

    private SimpleDateFormat sdf = new SimpleDateFormat(StorageUtils.getStringResource(R.string.date_format));

    public TweetAdapter(Context context) {
        this.context = context;
        this.tweetList = new ArrayList<Status>();
    }

    public void setTweetList(List<Status> tweetList) {
        this.tweetList = tweetList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return tweetList.size();
    }

    @Override
    public Object getItem(int position) {
        return tweetList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater li = LayoutInflater.from(context);
            view = li.inflate(R.layout.row_tweet_item, null);
        }

        ViewGroup.LayoutParams params = view.getLayoutParams();
        if (params != null) {
            params.height = DP_50;
        }
        setupRowElements(view, position);
        hideRowElements(view);

        return view;
    }

    private void setupRowElements(View view, final int position) {
        final Status tweet = tweetList.get(position);

        TextView tweetText = (TextView) view.findViewById(R.id.tweetText);
        tweetText.setText(tweet.getText());

        TextView tweeterText = (TextView) view.findViewById(R.id.tweeterText);
        tweeterText.setText("@" + tweet.getUser().getName());

        TextView tweetDateText = (TextView) view.findViewById(R.id.tweetDateText);
        tweetDateText.setText(sdf.format(tweet.getCreatedAt()));

        ImageView tweeterIcon = (ImageView) view.findViewById(R.id.tweeterIcon);
        tweeterIcon.setImageResource(R.drawable.earthquake_icon);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastOpenedPosition != position) {

                    collapseRowAndHideElements(lastOpenedView);

                    lastOpenedPosition = position;
                    lastOpenedView = v;

                    expandRowAndShowElements(v, tweet);

                } else {
                    collapseRowAndHideElements(v);
                    lastOpenedPosition = -1;
                }
            }
        });
    }

    private void expandRowAndShowElements(View v, Status tweet) {
        if (v != null) {
            loadTweeterIcon(v, tweet);
            resizeView(v, DP_50, DP_75);
            showRowElements(v);
        }
    }

    private void showRowElements(View v) {
        TextView tweeterText = (TextView) v.findViewById(R.id.tweeterText);
        tweeterText.setVisibility(View.VISIBLE);

        TextView tweetDateText = (TextView) v.findViewById(R.id.tweetDateText);
        tweetDateText.setVisibility(View.VISIBLE);

        ImageView tweeterIcon = (ImageView) v.findViewById(R.id.tweeterIcon);
        tweeterIcon.setVisibility(View.VISIBLE);
    }

    private void collapseRowAndHideElements(final View v) {
        if (v != null) {
            cancelLoaderIfRunning();
            resizeView(v, DP_75, DP_50);
            hideRowElements(v);
        }
    }

    private void hideRowElements(View v) {
        TextView tweeterText = (TextView) v.findViewById(R.id.tweeterText);
        tweeterText.setVisibility(View.GONE);

        TextView tweetDateText = (TextView) v.findViewById(R.id.tweetDateText);
        tweetDateText.setVisibility(View.GONE);

        ImageView tweeterIcon = (ImageView) v.findViewById(R.id.tweeterIcon);
        tweeterIcon.setVisibility(View.GONE);
    }

    private void resizeView(View v, int from, int to) {
        ResizeAnimation resizeAnimation = new ResizeAnimation(v, from, to);
        v.startAnimation(resizeAnimation);
    }

    private void loadTweeterIcon(View v, Status tweet) {

        try {
            String iconUrl = tweet.getUser().getProfileImageURL();

            ImageView tweeterIcon = (ImageView) v.findViewById(R.id.tweeterIcon);

            launchLoaderTask(tweeterIcon, iconUrl);

        } catch (NullPointerException e) {
            Log.e("QR", "Error getting twitter icon", e);
        }
    }

    private void launchLoaderTask(ImageView tweeterIcon, String iconUrl){
        cancelLoaderIfRunning();
        loadTweeterIconTask = new LoadTweeterIconTask(tweeterIcon, iconUrl);
        loadTweeterIconTask.execute();
    }

    public void cancelLoaderIfRunning() {
        if (loadTweeterIconTask != null && loadTweeterIconTask.getStatus() == AsyncTask.Status.RUNNING) {
            loadTweeterIconTask.cancel(true);
            loadTweeterIconTask = null;
        }
    }

    private class LoadTweeterIconTask extends AsyncTask<Void, Void, Bitmap> {

        private ImageView tweeterIcon;
        private String iconUrl;

        public LoadTweeterIconTask(ImageView tweeterIcon, String iconUrl) {
            this.tweeterIcon = tweeterIcon;
            this.iconUrl = iconUrl;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            return tryToDownloadTweeterBitmap(iconUrl);
        }

        protected void onPostExecute(final Bitmap result) {
            if (result != null) {
                setTweeterIcon(tweeterIcon, result);
            }
        }
    }

    private Bitmap tryToDownloadTweeterBitmap(String iconUrl) {
        try {
            return downloadTweeterBitmap(iconUrl);
        } catch (Exception e) {
            Log.e("QR", "Error loading tweeter icon", e);
        }
        return null;
    }

    private Bitmap downloadTweeterBitmap(String iconUrl) throws IOException {
        URL url = new URL(iconUrl);
        InputStream is = url.openConnection().getInputStream();
        return BitmapFactory.decodeStream(is);
    }

    private void setTweeterIcon(ImageView tweeterIcon, Bitmap bitmap){
        tweeterIcon.setImageBitmap(bitmap);
    }
}
