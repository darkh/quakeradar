package com.braula.quakeradar.animations;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AbsListView;

/**
 * Change view height animation
 * TODO: fine tuning
 * Created by Aleksander_Braula on 15-Dec-14.
 */
public class ResizeAnimation extends Animation {

    private View view;
    private float fromHeight;
    private float toHeight;

    public ResizeAnimation(View v, float fromHeight, float toHeight) {
        this.fromHeight = fromHeight;
        this.toHeight = toHeight;
        view = v;
        setDuration(300);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        float height = (toHeight - fromHeight) * interpolatedTime + fromHeight;

        AbsListView.LayoutParams p = (AbsListView.LayoutParams)view.getLayoutParams();
        p.height = (int) height;
        view.requestLayout();
    }
}
