package com.braula.quakeradar.enums;

import android.graphics.Color;

/**
 * Created by Aleksander_Braula on 2015-01-06.
 */
public enum QuakeMagnitude {
    I("#000000"),
    II_III("#acd8e9"),
    IV("#83d0da"),
    V("#7bc87f"),
    VI("#f9f518"),
    VII_VIII("#fac611"),
    IX("#f7100c"),
    X("#c80f0a");

    private String colorHash;

    private QuakeMagnitude(String colorHash) {
        this.colorHash = colorHash;
    }

    public String getColorHash() {
        return colorHash;
    }

    public int getColor(){
        return Color.parseColor(colorHash);
    }
}
