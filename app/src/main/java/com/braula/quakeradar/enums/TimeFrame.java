package com.braula.quakeradar.enums;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Interval;
import org.joda.time.Period;

/**
 * Created by Aleksander_Braula on 2014-12-23.
 */
public enum TimeFrame {
    HOURS_6 ("6 hours", new Period(Hours.SIX)),
    HOURS_12 ("12 hours", new Period(Hours.hours(12))),
    DAYS_1 ("1 day", new Period(Days.ONE)),
    DAYS_3 ("3 days", new Period(Days.THREE)),
    DAYS_7 ("1 week", new Period(Days.SEVEN));

    private String value;
    private Period period;

    TimeFrame(String value, Period period) {
        this.value = value;
        this.period = period;
    }

    @Override
    public String toString() {
        return value;
    }

    public Interval getInterval() {
        DateTime currentDate = new DateTime().toDateTime();
        return new Interval(currentDate.minus(period), currentDate);
    }
}
