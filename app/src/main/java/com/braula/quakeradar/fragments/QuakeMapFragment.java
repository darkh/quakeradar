package com.braula.quakeradar.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.braula.quakeradar.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import eu.darkh.library.utils.UiUtils;

/**
 * Fragment for map view
 * Created by Aleksander_Braula on 2014-12-12.
 */
public class QuakeMapFragment extends Fragment {

    private double latitude;
    private double longitude;

    private static View view;

    public void setCoordinates(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        tryToInitMapView(inflater, container);
        return view;
    }

    private void tryToInitMapView(LayoutInflater inflater, ViewGroup container) {
        try {
            view = inflater.inflate(R.layout.fragment_quake_map, container, false);
        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tryToInitGoogleMap();
    }

    private void tryToInitGoogleMap() {
        try {
            initGoogleMap();
        } catch (NullPointerException e) {
            Log.e("QR", "Map error: " + e.getMessage(), e);
            UiUtils.showToastWithText(R.string.map_error);
        }
    }

    private void initGoogleMap() {
        FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = (MapFragment) fragmentManager.findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                addMarkerAndMove(googleMap);
            }
        });
    }

    private void addMarkerAndMove(GoogleMap googleMap) {
        LatLng latLng = new LatLng(latitude, longitude);

        googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.earthquake_icon)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(3));
    }
}
