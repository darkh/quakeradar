package com.braula.quakeradar.fragments;

import android.app.ListFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.braula.quakeradar.R;
import com.braula.quakeradar.adapters.TweetAdapter;
import com.braula.quakeradar.utils.TwitterUtils;

import java.util.List;

import twitter4j.GeoLocation;

/**
 * Fragment for tweet list
 * Created by Aleksander_Braula on 2014-12-12.
 */
public class QuakeTweetsFragment extends ListFragment {

    private double latitude;
    private double longitude;

    private TweetAdapter tweetAdapter;

    private ProgressBar loadingIndicator;
    private TextView infoText;

    private LoadTweetsTask loadTweetsTask;
    private boolean isTweetDataLoaded = false;

    public void setCoordinates(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tweetAdapter = new TweetAdapter(getActivity());
        setListAdapter(tweetAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quake_tweets, container, false);

        loadingIndicator = (ProgressBar) view.findViewById(R.id.loadingIndicator);
        infoText = (TextView) view.findViewById(R.id.infoText);

        return view;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!isTweetDataLoaded) {
            launchLoaderTask();
        }
    }


    @Override
    public void onDestroyView () {
        super.onDestroyView();
        tweetAdapter.cancelLoaderIfRunning();
        cancelLoaderIfRunning();
    }

    private void launchLoaderTask() {
        cancelLoaderIfRunning();
        loadTweetsTask = new LoadTweetsTask();
        loadTweetsTask.execute();
    }

    private void cancelLoaderIfRunning() {
        if (loadTweetsTask != null && loadTweetsTask.getStatus() == AsyncTask.Status.RUNNING) {
            loadTweetsTask.cancel(true);
            loadTweetsTask = null;
        }
    }

    private class LoadTweetsTask extends AsyncTask<Void, Void, List<twitter4j.Status>> {

        protected void onPreExecute (){
            showLoading();
        }

        protected List<twitter4j.Status> doInBackground(Void... params) {
            GeoLocation geoLocation = new GeoLocation(latitude, longitude);
            return TwitterUtils.tryToGetTweetListByGeoLocation(geoLocation);
        }

        protected void onPostExecute(final List<twitter4j.Status> tweetList) {
            Log.d("QR", "Tweets no " + tweetList.size());

            hideLoading();

            if (tweetList.size() > 0) {
                isTweetDataLoaded = true;
                tweetAdapter.setTweetList(tweetList);
            } else {
                showNoDataText();
            }
        }
    }

    private void showLoading() {
        hideListView();
        showLoadingIndicator();
        hideNoDataText();
    }

    private void hideLoading() {
        hideLoadingIndicator();
        showListView();
    }

    private void showListView(){
        getListView().setVisibility(View.VISIBLE);
    }

    private void hideListView(){
        getListView().setVisibility(View.GONE);
    }

    private void showNoDataText(){
        infoText.setVisibility(View.VISIBLE);
    }

    private void hideNoDataText(){
        infoText.setVisibility(View.GONE);
    }

    private void showLoadingIndicator() {
        loadingIndicator.setVisibility(View.VISIBLE);
    }

    private void hideLoadingIndicator() {
        loadingIndicator.setVisibility(View.GONE);
    }

}
