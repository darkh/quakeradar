package com.braula.quakeradar.model;

import com.braula.quakeradar.R;

import org.joda.time.DateTime;

import eu.darkh.library.utils.StorageUtils;

/**
 * Quake item class
 * Created by Aleksander_Braula on 12-Dec-14.
 */
public class Quake {

    private String id;
    private long time;
    private double latitude;
    private double longitude;
    private String tsunami;
    private double magnitude;

    private QuakeProperties properties;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getStringDate() {
        DateTime dateTime = new DateTime(time);
        return dateTime.toString(StorageUtils.getStringResource(R.string.date_format));
    }

    public DateTime getDateTime() {
        return new DateTime(time);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isTsunami() {
        return tsunami != null;
    }

    public String getTsunami() {
        return tsunami;
    }

    public void setTsunami(String tsunami) {
        this.tsunami = tsunami;
    }

    public double getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(double magnitude) {
        this.magnitude = magnitude;
    }

    public QuakeProperties getProperties() {
        return properties;
    }

    public void setProperties(QuakeProperties properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
                .append("id: ").append(id)
                .append(", region: ").append(properties.getRegion())
                .append(", coordinates: ").append(latitude).append(", ").append(longitude)
                .append(", tsunami: ").append(isTsunami())
                .append(", magnitude: ").append(magnitude)
                .append(", time: ").append(getStringDate());

        return sb.toString();
    }
}
