package com.braula.quakeradar.model;

/**
 * Created by Aleksander_Braula on 2015-01-02.
 */
public class QuakeProperties {
    private String region;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
