package com.braula.quakeradar.utils;

import android.util.Log;

import com.braula.quakeradar.R;
import com.braula.quakeradar.model.Quake;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import eu.darkh.library.utils.NetworkUtils;
import eu.darkh.library.utils.StorageUtils;

/**
 * Utils for loading quake data
 * Created by Aleksander_Braula on 2014-12-12.
 */
public class QuakeDataUtils {

    public static final String SHARED_PREFERENCES_QUAKE_ITEM = "quakeList";

    public static ArrayList<Quake> tryToGetQuakeList(){
        try {
            return getQuakeList();
        } catch (Exception e) {
            Log.e("QR", "error: " + e.getMessage(), e);
        }
        return new ArrayList<Quake>();
    }

    private static ArrayList<Quake> getQuakeList(){
        String json;

        if (NetworkUtils.hasNetworkConnection()) {
            Log.d("QR", "Retrieving data from internet");
            json = tryToDownloadDataFromURL(StorageUtils.getStringResource(R.string.quake_feed_url));
            StorageUtils.putStringInSharedPreferences(SHARED_PREFERENCES_QUAKE_ITEM, json);
        } else {
            Log.d("QR", "Retrieving data from storage");
            json = StorageUtils.getStringFromSharedPreferences(SHARED_PREFERENCES_QUAKE_ITEM);
        }

        return tryToGetQuakeListFromJSON(json);
    }

    private static String tryToDownloadDataFromURL(String urlStr) {
        try {
            return downloadDataFromURL(urlStr);
        } catch (Exception e) {
            Log.e("QR", "Connection problem", e);
        }
        return "";
    }

    private static String downloadDataFromURL(String urlStr) throws IOException {
        Log.d("QR", "Download data from: " + urlStr);

        URL url = new URL(urlStr);

        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        InputStream inputStream = urlConnection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line).append("\n");
        }

        inputStream.close();
        urlConnection.disconnect();

        return sb.toString();
    }

    private static ArrayList<Quake> tryToGetQuakeListFromJSON(String json) {
        try {
            return getQuakeListFromJSON(json);
        } catch (JsonParseException e) {
            Log.e("QR", "Deserialization problem", e);
        }
        return new ArrayList<Quake>();
    }

    private static ArrayList<Quake> getQuakeListFromJSON(String json) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Type type = new TypeToken<ArrayList<Quake>>(){}.getType();
        return gson.fromJson(json, type);
    }
}
