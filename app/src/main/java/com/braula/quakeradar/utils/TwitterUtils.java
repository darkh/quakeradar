package com.braula.quakeradar.utils;

import android.util.Log;

import com.braula.quakeradar.R;

import java.util.ArrayList;
import java.util.List;

import eu.darkh.library.utils.StorageUtils;
import twitter4j.GeoLocation;
import twitter4j.Query;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Aleksander_Braula on 2014-12-12.
 */
public class TwitterUtils {

    private static Twitter twitter = null;
    private static final int TWEET_LIMIT = 20;
    private static final int TWEET_RADIUS = 100;

    public static List<Status> tryToGetTweetListByGeoLocation(GeoLocation geoLocation) {
        try {
            return getTweetListByGeoLocation(geoLocation);
        } catch (Exception e) {
            Log.e("QR", "Tweeter error", e);
        }

        return new ArrayList<Status>();
    }

    private static List<Status> getTweetListByGeoLocation(GeoLocation geoLocation) throws TwitterException {
        initTwitter();

        Query query = prepareTwitterQuery(geoLocation);

        return twitter.search(query).getTweets();
    }

    private static void initTwitter() {
        if (twitter == null) {
            ConfigurationBuilder builder = new ConfigurationBuilder();

            builder.setOAuthConsumerKey(StorageUtils.getStringResource(R.string.twitter_consumer_key));
            builder.setOAuthConsumerSecret(StorageUtils.getStringResource(R.string.twitter_consumer_secret));

            AccessToken accessToken = new AccessToken(
                    StorageUtils.getStringResource(R.string.twitter_access_token),
                    StorageUtils.getStringResource(R.string.twitter_access_token_secret));

            twitter = new TwitterFactory(builder.build()).getInstance(accessToken);
        }
    }

    private static Query prepareTwitterQuery(GeoLocation geoLocation) {
        Query query = new Query();
        query.setGeoCode(geoLocation, TWEET_RADIUS, Query.Unit.km);
        query.setCount(TWEET_LIMIT);

        return query;
    }
}
